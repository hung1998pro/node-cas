# Express CAS Authentication

- `bounce`: Redirects an unauthenticated client to the CAS login page and then back to the requested page.
- `block`: Completely denies access to an unauthenticated client and returns a 401 response.

It also provides two route endpoint functions:

- `bounce_redirect`: Acts just like `bounce` but once the client is authenticated they will be redirected to the provided _returnTo_ query parameter.
- `logout`: De-authenticates the client with the Express server and then redirects them to the CAS logout page.

## Installation

    npm install
    npm start

## Setup

```javascript
var CASAuthentication = require('cas-authentication');

var cas = new CASAuthentication({
    cas_url: 'https://login.vng.com.vn/sso',
    service_url: 'https://localhost:3000',
    cas_version     : '3.0',
    session_name: 'cas_user',
    session_info: 'cas_userinfo',
    renew           : false,
    is_dev_mode     : false,
    dev_mode_user   : '',
    dev_mode_info   : {},
    destroy_session : false

});
```

## Usage

URL:

- `/app`: Unauthenticated clients will be redirected to the CAS login and then back to

- `/api`: Unauthenticated clients will receive a 401 Unauthorized response instead of the JSON data.

- `/api/user`: retrieve your own local user records based on authenticated CAS username.

- `/logout`: redirect the client to the CAS logout page.